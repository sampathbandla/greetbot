# GreetBot

GreetBot will greet new people joining your discord server with happy messages.

## Installation

To install the bot in your discord server, [read the wiki](https://gitlab.com/codecarrot/greetbot/wikis/home).

## ⚠ Cuation

Once the server is stop or crashed, all the messages you have saved might lost. We suggest you to keep the messages save somewhere. We are working on feature to [import messages in bulk](https://gitlab.com/codecarrot/greetbot/issues/1).

## Issues

Found a problem or missing feature? Issues and feature requests can be submitted in the [GitLab repository](https://gitlab.com/codecarrot/greetbot).

## Contribute

PRs welcome! Please see our [Contribution](https://gitlab.com/codecarrot/greetbot/wikis/contribution) for more details.